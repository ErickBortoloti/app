# Imagem base do Python - leve
FROM python:3.8-slim

# Define o diretório de trabalho no container
WORKDIR /app

# Copia apenas o arquivo necessário
COPY app.py /app/

# Exponha a porta em que a aplicação estará em execução
EXPOSE 80

#LIMPEZA

RUN rm -rf /var/lib/apt/lists/* /root/.cache/pip

# Comando para executar a aplicação quando o contêiner iniciar
CMD ["python", "app.py"]

